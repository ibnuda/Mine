## Nothing. Really.

- The game is done in a `Field`
- The `Field` has two dimensions: `Width` and `Length`.
- The unit of `Width` and `Length` is in **integer** that bigger than **zero**
- The unit of `Width` times `Length` in this game will be called `Tile`.
- There are a certain number of `Bomb`s.
- The total amount of `Bomb` is more than **0** (zero) and less than `Width` times `Length`.
- Each `Bomb` is put **randomly** in a `Coordinate`-d manner.
- The `Coordinate` has **two** parts:
  - `X` part which correspond with the `Width` dimension.
  - `Y` part which correspond with the `Length` dimension.
- There are **two** types of `Tile`:
  - `CoveredTile`
  - `CheckedTile`
- In each `CoveredTile`, there's a probability **one** and **only one** `Bomb` in it.
- Initially, the `state` of the `CoveredTile` is `Unknown`.
- There are two possible `Act`s on a `CoveredTile`:
  - `Flag`
  - `Check`
- `Flag` is an action that marks a `CoveredTile` that may has a `Bomb` in it. 
- After being `Flag`-ed the `state` of a `Tile` is `CoveredFlagged`.
- When an `Unknown` `CoveredTile` `Flag`-ed twice, the state of the `Tile` will be back to `Unknown`.
- `Check` is an action that checks a `Tile` whether it contains a `Bomb` or a safe one.
- There are two possible states of a `Tile` after being `Check`-ed:
  - `Safe`
  - `Explode`
- When the result of a `CheckedTile` is a `Safe`, that `Tile` will show number of `Bomb` on its `Neighboor`ing `Tile`s.
- `Neighboor` of a `Tile` is the `Tile`s that surrounds a `Tile`.

## An Example
The following picture is an `Initiated` `Field` with `4` units of `Width` and `5` units `Length`.
Each and every `Tile` on the aformentioned `Field` is in `Unknown` State while amount of `Bomb` is **5**.
To ease the explanation, each `Tile` in that `Field` will be named with an alphabet.
```
.---. .---. .---. .---.
| a | | b | | c | | d |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| e | | f | | g | | h |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| i | | j | | k | | l |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| m | | n | | o | | p |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| q | | r | | s | | t |
'---' '---' '---' '---' 
Bomb: 5
```
`Tile` **a** is located in `Coordinate` `0, 0` while `Tile` **i** is in `Coordinate` `0, 2` and `Tile` **p** is in `Coordinate` `4, 3` and so on, so forth.


When `Tile` **n** being `Flag`-ed, the state of the `Field` will be changed to:
```
.---. .---. .---. .---.
| a | | b | | c | | d |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| e | | f | | g | | h |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| i | | j | | k | | l |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| m | | + | | o | | p |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| q | | r | | s | | t |
'---' '---' '---' '---' 
Bomb: 5
```

The symbol `+` on the `Tile` which previously named **n** means that the state of `Tile` **n** is `Flagged`.

And when the `Tile` **q** being `Check`-ed and there's no `Bomb` underneath it, the state of the `Field` changed to:
```
.---. .---. .---. .---.
| a | | b | | c | | d |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| e | | f | | g | | h |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| i | | j | | k | | l |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| m | | + | | o | | p |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| 1 | | r | | s | | t |
'---' '---' '---' '---' 
Bomb: 4
```
The symbol `1` on the `Tile` which previously named **q** means that **one** of the `neighboor`s of `Tile` **q** has a `Bomb`.

When `Tile` **r** being `Check`-ed and there's a `Bomb` underneath it, the state of the `Field` changed to:
```
.---. .---. .---. .---.
| a | | b | | c | | d |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| e | | f | | g | | h |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| i | | j | | k | | l |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| m | | + | | o | | p |
'---' '---' '---' '---' 
.---. .---. .---. .---.
| 1 | | X | | s | | t |
'---' '---' '---' '---' 
Bomb: 5
```
The symbol 'X' on the `Tile` which previously named **r** means the `Bomb` under the previous `Tile` has **exploded** and the game ends. 
