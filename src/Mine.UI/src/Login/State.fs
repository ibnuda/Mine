module Login.State

open Elmish
open Types

let init () : Model * Cmd<Msg> =
  Password "", []

let update message model : Model * Cmd<Msg> =
  match message with
  | Something (username, password) ->
    Password password, []
