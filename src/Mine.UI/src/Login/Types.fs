module Login.Types

open System

type Model =
  | Username of string
  | Password of string

type Msg =
  | Something of Username : string * Password : string
