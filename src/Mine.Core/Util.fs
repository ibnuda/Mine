namespace Mine.Core

open System

module Util =

  let rand = Random ()
  let randInt = rand.Next
  let randFloat () = rand.NextDouble ()

  let isAPositive a = a > 0
  let isABPositive a b = a > 0 && b > 0
  let isReasonable a b c = a * b <= c

  let isReasonableGame maxa maxb maxamount =
       isAPositive maxamount
    && isABPositive maxa maxb
    && isReasonable maxa maxb maxamount

  let generatePositiveTuples maxA maxB maxAmount =
    let tuples = []

    let rec generate first second alreadyInList theList limit =
      if alreadyInList < limit then
        let a = randInt first
        let b = randInt second
        if List.contains (a, b) theList then
          generate first second alreadyInList theList limit
        else
          generate first second (alreadyInList + 1) ((a, b) :: theList) limit
      else
        theList

    let amount =
      if isReasonableGame maxA maxB maxAmount then
        maxAmount
      else
        0

    generate 0 maxA maxB tuples amount
