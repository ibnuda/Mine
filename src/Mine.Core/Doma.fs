namespace Mine.Core

open System

open Chessie
open Chessie.ErrorHandling

open Mine.Core.Errs
open Mine.Core.Util

module Doma =

  type Tile =
    | Covered of CoveredTile
    | Digged of DiggedTile
  and CoveredTile =
    | Unknown
    | Flagged
  and DiggedTile =
    | Safe of NeigbooringBomb : int
    | Bomb
    | Nothing

  type Progress =
    | Digging
    | Finished
    | Dead

  type Coordinate =
    { X : int
      Y : int }
    static member FromTuple (x, y) =
      { X = x
        Y = y }
    static member ToTuple (coordinate : Coordinate) =
      coordinate.X, coordinate.Y

  type Field =
    { Progress : Progress
      Tiles : Tile [ , ] }

    static member Empty () =
      { Progress = Progress.Digging
        Tiles = Array2D.init 1 1 (fun _ _ -> Tile.Covered CoveredTile.Unknown) }

    static member Create (width, length) =
      { Progress = Progress.Digging
        Tiles = Array2D.init width length (fun _ _ -> Tile.Covered CoveredTile.Unknown) }

    member __.ChangeTile (coordinate : Coordinate, uncoveredTile : DiggedTile) =
      let x, y = Coordinate.ToTuple coordinate
      __.Tiles.[x, y] <- Tile.Digged uncoveredTile
      __

  type MineField =
    { Id : Guid
      Field : Field
      Mines : Coordinate list }

    static member Empty () =
      { Id = System.Guid.Empty
        Field = Field.Empty ()
        Mines = [] }

    static member Create (guid, width, length, bombCount) =
      { Id = guid
        Field = Field.Create (width, length)
        Mines =
          generatePositiveTuples width length bombCount
          |> List.map Coordinate.FromTuple }

    member __.IsCoordinateInside (coordinate : Coordinate) =
      let x, y = coordinate.X, coordinate.Y
      let fX, fY = __.Field.Tiles.Length, __.Field.Tiles.Rank
      x <= fX && y <= fY && x >= 0 && y >= 0

    member __.BombsInSurroundingTiles (coordinate : Coordinate) =
      match __.IsCoordinateInside coordinate with
      | true ->
        0
      | _ ->
        0

    member __.DigATile (coordinate : Coordinate) =
      match __.IsCoordinateInside coordinate with
      | true ->
        if List.contains coordinate __.Mines then
          DiggedTile.Bomb
        else
          DiggedTile.Safe (__.BombsInSurroundingTiles coordinate)
      | false ->
        DiggedTile.Nothing

  type TheEnd =
    { Id : Guid
      Cleared : bool }
