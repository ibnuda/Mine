namespace Mine.Core

open System

module Errs =

  type Errs =
    | NotPossibleValue
    | NotPossibleGame
