namespace Mine.Core

open System

open Mine.Core.Doma

module Comm =

  type Command =
    | CreateMineField of Width : int * Length : int * BombCount : int
    | FlagMineField of MFId : Guid * Coord : Coordinate
    | DigMineField of MFId : Guid * Coord : Coordinate
    | RageQuit of MFId : Guid
