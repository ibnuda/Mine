namespace Mine.Core

open System

open Mine.Core.Doma

module Even =

  type Even =
    | Created of ResMF : MineField
    | Flagged of MFId : Guid * Coord : Coordinate
    | Digged of MFId : Guid * Coord : Coordinate * Res : DiggedTile
