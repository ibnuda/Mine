namespace Mine.Core

open System

open Chessie.ErrorHandling

open Mine.Core.Doma
open Mine.Core.Errs
open Mine.Core.Even
open Mine.Core.Comm
open Mine.Core.Stat

module Hand =

  let handCreateGame =
    function
    | NonExistent gameId ->
      ()
    | StillBeingDigged(mF) -> failwith "Not Implemented"
    | Ended(resMF) -> failwith "Not Implemented"
