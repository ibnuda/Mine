namespace Mine.Core

open System

open Mine.Core.Doma
open Mine.Core.Even

module Stat =

  type State =
    | NonExistent of MFId : Guid
    | StillBeingDigged of MF : MineField
    | Ended of ResMF : TheEnd

  let apply state event =
    match state, event with
    | NonExistent _, Created minefield ->
      StillBeingDigged minefield
    | StillBeingDigged minefield, Flagged (_, coord) ->
      StillBeingDigged minefield
    | StillBeingDigged minefield, Digged (id, coord, diggedTile) ->
      StillBeingDigged minefield
    | _ ->
      state
