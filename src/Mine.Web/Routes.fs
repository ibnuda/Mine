namespace Mine.Web

module Routes =

  open System

  open Freya.Core
  open Freya.Core.Operators
  open Freya.Machines.Http
  open Freya.Routers.Uri.Template
  open Freya.Types.Http

  open Handlers

  let routeHello =
    freyaMachine {
      methods [ GET; POST ]
      handleOk (Represent.text <!> answerHandler)
    }

  let routeLost =
    freyaMachine {
      methods [ GET; POST ]
      handleOk (Represent.text "lost")
    }

  let routeList =
    freyaRouter {
      resource "{/hello}" routeHello
      resource "{/lost*}" routeLost
    }
