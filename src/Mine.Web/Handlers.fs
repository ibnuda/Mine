namespace Mine.Web

module Handlers =

  open System

  open Freya.Core
  open Freya.Core.Operators
  open Freya.Optics.Http

  open Misc
  open Processors

  let answerHandler =
    Freya.memo (urlValue "hello" >>= fun hello -> (answerMe >> Freya.fromAsync) hello)
