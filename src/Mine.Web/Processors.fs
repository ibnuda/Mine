namespace Mine.Web

open System

module Processors =

  type Answer =
    // Answer : takes an Option<string> as an argument and return a string
    // as the result.
    | Answer of AsyncReplyChannel<string> * string option

  let processor (mailbox : MailboxProcessor<Answer>) =

    let reply (channel : AsyncReplyChannel<_>) request =
      channel.Reply request
      request

    let answer channel strOption =
      match strOption with
      | Some str -> str
      | _ -> ""
      |> reply channel

    let rec loop _ =
      async.Bind (mailbox.Receive (),
                  function
                  | Answer (channel, request) -> loop (answer channel request))

    loop ""

  let serverState = MailboxProcessor.Start processor

  let answerMe request =
    serverState.PostAndAsyncReply (fun channel -> Answer (channel, request))
