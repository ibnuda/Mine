namespace Mine.Web

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http

open Freya.Core
open Freya.Core.Operators
open Freya.Polyfills.Kestrel
open Freya.Routers.Uri.Template

module KInterop =

  module ApplicationBuilder =
    let inline useRouteListFromFreya f (app : IApplicationBuilder) =
      let routes = UriTemplateRouter.Freya f
      let pipeline = Polyfill.kestrel >?= routes
      let midFunc : OwinMidFunc = OwinMidFunc.ofFreya pipeline
      app.UseOwin (fun p -> p.Invoke midFunc)

    let inline complete (_ : IApplicationBuilder) = ()

  module WebHost =
    let create () = WebHostBuilder().UseKestrel()
    let bindTo urls (b:IWebHostBuilder) = b.UseUrls urls
    let configure (f : IApplicationBuilder -> IApplicationBuilder) (b:IWebHostBuilder) =
      b.Configure (System.Action<_> (f >> ignore))
    let build (b:IWebHostBuilder) = b.Build()
    let run (wh:IWebHost) = wh.Run()
    let buildAndRun : IWebHostBuilder -> unit = build >> run
