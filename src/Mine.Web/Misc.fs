namespace Mine.Web

module Misc =

  open System
  open System.IO

  open Freya.Core
  open Freya.Core.Operators
  open Freya.Optics.Http
  open Freya.Routers.Uri.Template

  let urlValue url =
    Route.atom_ url
    |> Freya.Optic.get

  let urlListValue urlList =
    Route.list_ urlList
    |> Freya.Optic.get

  let raw =
    function
    | (stream : Stream) -> (new StreamReader(stream, Text.Encoding.UTF8)).ReadToEnd ()
    <!> (Freya.Optic.get Request.body_)
