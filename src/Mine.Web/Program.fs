﻿open System

open Mine.Web.KInterop
open Mine.Web.Routes

[<EntryPoint>]
let main argv =
  let configureApp =
    ApplicationBuilder.useRouteListFromFreya routeList

  WebHost.create ()
  |> WebHost.bindTo [| "http://*:5000" |]
  |> WebHost.configure configureApp
  |> WebHost.buildAndRun

  0
