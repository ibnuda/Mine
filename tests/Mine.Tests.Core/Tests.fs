module Tests

open System
open Xunit

open Mine.Core.Doma

open Datas

[<Fact>]
let ``Create Field As Expected`` () =
  let createdField = Field.Create (10, 10)
  Assert.Equal(expectedInitiatedField, createdField)

[<Fact>]
let ``Create Empty Minefield`` () =
  let createdMinefield = MineField.Create (Guid.Empty, 10, 10, 0)
  Assert.Equal(expectedInitiatedMinefield, createdMinefield)
