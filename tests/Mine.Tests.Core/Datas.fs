module Datas

open System
open Mine.Core.Doma

let emptyField = Field.Empty ()
let expectedInitiatedField =
  { Progress = Progress.Digging
    Tiles = Array2D.init 10 10 (fun _ _ -> Tile.Covered CoveredTile.Unknown) }

let expectedInitiatedMinefield =
  { Id = Guid.Empty
    Field = expectedInitiatedField
    Mines = [] }
